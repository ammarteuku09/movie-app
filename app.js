const express = require('express')
const app = express()
const port = 3000
const routes = require('./routes/index.js')
const errorHandler = require('./errorHandler')
require('dotenv').config()

const morgan = require('morgan')
const sentry = require('@sentry/node')

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use((req, res, next) => {
    req.sentry = sentry
    next()
  })

app.use(morgan('dev'))
app.use(routes)
app.use(errorHandler)

// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })

module.exports = app