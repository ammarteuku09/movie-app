const {WishlLists, Movies} = require("../models");
const movies = require("../models/movies");

class MoviesAndWishListController {
    static async getMovies (req, res, next) {
        try {
          const movies = await Movies.findAll()
          res.status(200).json(movies);
        } catch (err) {
          next(err);
        }
       }
    static async getMovieWishList (req, res, next) {
        try {
          const wishlist = await WishlLists.findAll({
              where:{
                  user_id:req.user.id
              },
              include:[
                  {
                    model:Movies,
                    where:{
                        id:wishlist.movie_id,
                    }
                  }
              ]
          })
          res.status(200).json(movies);
        } catch (err) {
          next(err);
        }
       }
       
       static async createWishList (req, res) {
        await WishlLists.create({
         movie_id: req.body.movie_id,
         user_id: req.user.id,
        })
  
        res.status(201).json({
          message: 'Successfully create user'
        })
     }

     static async updateWishList(req, res, next) {
        try{
            const wishlist = await WishlLists.update({
                movie_id: req.body.movie_id,
            },{
             where: {
               id: req.params.id
             },
             returning: true,
           })
           res.status(200).json(wishlist[1][0])
         } catch(err) {
           next(err)
         }
        }

        static async deleteWishList (req, res, next) {
            try {
              await WishlLists.destroy({
                where: {
                  id: req.params.id
                }
              })
              res.status(200).json('Succesfully delete product')
            } catch(err) {
              next(err);
            }
          }
}

module.exports = MoviesAndWishListController;