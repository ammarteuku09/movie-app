const { Users } = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { OAuth2Client } = require('google-auth-library')
const axios = require('axios')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)
const cloudinary = require('../services/cloudinary');
const fs = require('fs')
const sendEmail = require('../services/mailer');

class AuthController {
  static async login(req, res, next) {
    try {
      if (req.body.email && req.body.password) {
        const user = await Users.findOne({
          where: {
            email: req.body.email
          }
        })
  
        if (!user) {
          throw {
            status: 401,
            message: 'Invalid email or password'
          }
        }
        if (bcrypt.compareSync(req.body.password, user.password)) {
          const token = jwt.sign({
            id: user.id,
            email: user.email
          }, 'qweqwe')
         res.status(200).json({
            token
          })
        } else {
          throw {
            status: 401,
            message: 'Invalid email or password'
          }
        }
      } else if (req.body.google_id_token) {
        // melakukan verifikasi id token
        const payload = await client.verifyIdToken({
          idToken: req.body.google_id_token,
          audience: process.env.GOOGLE_CLIENT_ID
        })
        // mencari email dari google di database
        const user = await Users.findOne({
          where: {
            email: payload.payload.email
          }
        })

        if (user) {
          const token = jwt.sign({
            id: user.id,
            email: user.email
          }, 'qweqwe')
          await sendEmail('ammarteuku09@gmail.com', req.user.email, 'Your Account Has Registered', `Hai ${req.body.name} your account has been created at ${new Date}`)
          res.status(200).json({ token })
        } else {
          const createdUser = await Users.create({
            email: payload.payload.email
          })
          const token = jwt.sign({
            id: createdUser.id,
            email: createdUser.email,
            role:createdUser.role
          }, config.SECRET)
          res.status(200).json({ token })
        }
        // mendaftarkan secara otomatis, jika user belom ada di database
      } else if (req.body.facebook_id_token) {
        const response = await axios.get(`https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${req.body.facebook_id_token}`)
        // mencari user di database
        const user = await Users.findOne({
          where: {
            email: response.data.email
          }
        })

        if (user) {
          const token = jwt.sign({
            id: user.id,
            email: user.email,
            role: user.role
          }, config.SECRET)
          await sendEmail('ammarteuku09@gmail.com', req.user.email, 'Your Account Has Registered', `Hai ${req.body.name} your account has been created at ${new Date}`)
          res.status(200).json({ token })
        } else {
          const createdUser = await Users.create({
            email: response.data.email
          })

          const token = jwt.sign({
            id: createdUser.id,
            email: createdUser.email,
            role: createdUser.role,
          },config.SECRET)
          res.status(200).json({ token })
        }
      }
    } catch (err) {
      next(err)
    }
  }

  static async register (req, res) {
    const result = await cloudinary.uploader.upload(req.file.path);
    // menghapus gambar di dalam folder public/src
    fs.unlinkSync(req.file.path)

    const user = await Users.create({
        fullName: req.body.fullName,
        email: req.body.email,
        password: req.body.password,
        role:"user",
        image: result.secure_url
    })

    if (user){
      await sendEmail('ammarteuku09@gmail.com', req.user.email, 'Your Account Has Registered', `Hai ${req.body.name} your account has been created at ${new Date}`)
      res.status(201).json({
        message: 'Successfully create user'
      })
    }
 }

 static async sendForgotPasswordToken(req, res, next) {
  try {
    const user = await Users.findOne({
      where: {
        email: req.body.email
      }
    })

    if (!user) {
      throw {
        status: 400,
        message: 'Invalid email'
      }
    } else {
      const otp = otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false });
      const salt = bcrypt.genSaltSync(10)
      const hash = bcrypt.hashSync(otp, salt)
      await Users.update({
        forgot_pass_token: hash,
        forgot_pass_token_expired_at: new Date(new Date().getTime() + 5 * 60000)
      }, {
        where: {
          email: req.body.email
        }
      })
      const html = `
      <pre>
      Token Anda: ${otp} <br>
      Email terbuat otomatis pada ${new Date()}
      <pre>
      `
      await sendEmail('ammarteuku09@gmail.com', req.body.email, html, null, 'Your Forgot Password Token')
      res.status(200).json({
        message: 'Succesfully send email'
      })
      
    }
  } catch(err) {
    next(err)
  }
}
}

module.exports = AuthController
