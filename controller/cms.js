const  {Movies, Users} = require("../models");
const movies = require("../models/movies");
const bcrypt = require('bcryptjs')
const config = require('../config/env').get(process.env.NODE_ENV);
const jwt = require('jsonwebtoken')
const cloudinary = require('../services/cloudinary');
const fs = require('fs')

class CmsController {
    static async createMovie (req, res, next) {
      try {
        const result = await cloudinary.uploader.upload(req.file.path);
        // menghapus gambar di dalam folder public/src
        fs.unlinkSync(req.file.path)

        await Movies.create({
         genre: req.body.genre,
         movie_name: req.body.movie_name,
         image: result.secure_url
        })

        const user = await Users.findAll({
          where:{
            role:"user"
          }
        })

        if (user){
          await sendEmail('ammarteuku09@gmail.com', req.user.email, `Film ${req.body.movie_name} sudah tayang!!`, `Film ${req.body.movie_name} sudah tayang di seluruh bioskop di Indonesia`)
        }

        res.status(201).json({
          message: 'Successfully create movie'
        })
      } catch (error) {
        next(err)
      }
     }

     static async updateMovie(req, res, next) {
        try{
            const movie = await movies.update({
                genre: req.body.genre,
                movie_name: req.body.movie_name,
            },{
             where: {
               id: req.params.id
             },
             returning: true,
           })
           res.status(200).json(wishlist[1][0])
         } catch(err) {
           next(err)
         }
        }

        static async deleteMovie (req, res, next) {
            try {
              await movies.destroy({
                where: {
                  id: req.params.id
                }
              })
              res.status(200).json('Succesfully delete product')
            } catch(err) {
              next(err);
            }
          }

          static async createAdmin (req, res){
            await Users.create({
                fullName: req.body.fullName,
                email: req.body.email,
                password: req.body.password,
                role:"admin"
            })
        
            res.status(201).json({
              message: 'Successfully create user'
            })
          }

          static async login (req, res, next) {
            try {
              const user = await Users.findOne({
                where: {
                 email: req.body.email
                }
              })
              if (!user) {
                throw {
                  status: 401,
                  message: 'Invalid email or password'
                }
              } else {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                  // mengeluarkan token
                  let token = jwt.sign({ id: user.id, email: user.email, role: user.role}, config.SECRET);
                  res.status(200).json({
                    message: 'Login success',
                    token,
                  })
                  console.log(token)
                } else {
                  throw {
                    status: 401,
                    message: 'Invalid email or password'
                  }
                }
              }
             } catch (error) {
              next(error);
             }
           }
}

module.exports=CmsController;