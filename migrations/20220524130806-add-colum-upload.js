'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.addColumn('Users', 'image', { type: Sequelize.STRING });
     await queryInterface.addColumn('Movies', 'video', { type: Sequelize.STRING });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.remove('Users', 'image', { type: Sequelize.STRING });
     await queryInterface.remove('Movies', 'video', { type: Sequelize.STRING });
  }
};
