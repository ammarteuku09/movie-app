'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.addColumn('Users', 'forgot_pass_token', { type: Sequelize.STRING });
     await queryInterface.addColumn('Users', 'forgot_pass_token_expired_at', { type: Sequelize.DATE });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.remove('Users', 'forgot_pass_token', { type: Sequelize.STRING });
     await queryInterface.remove('Users', 'forgot_pass_token_expired_at', { type: Sequelize.DATE });
  }
};
