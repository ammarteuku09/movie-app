'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Movies.hasMany(models.WishlLists, { foreignKey: 'movie_id', as: 'WishlLists'})
    }
  }
  Movies.init({
    genre: DataTypes.STRING,
    movie_name: DataTypes.STRING,
    video:DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Movies',
  });
  return Movies;
};