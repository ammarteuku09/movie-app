'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcryptjs');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Users.hasMany(models.WishlLists, { foreignKey: 'user_id', as: 'WishLists'})
    }
  }
  Users.init({
    fullName: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    role:DataTypes.STRING,
    image:DataTypes.STRING,
    forgot_pass_token: DataTypes.STRING,
    forgot_pass_token_expired_at: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Users',
  });
  Users.addHook('beforeCreate', (user, options) => {

    // enkripsi / hash si password
    const salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(user.password, salt);
    // user.password = hash;
  });
  return Users;
};