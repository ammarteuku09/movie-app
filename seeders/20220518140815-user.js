'use strict';
const bcrypt = require('bcryptjs')
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const salt = bcrypt.genSaltSync(10)
     const hash = bcrypt.hashSync("12345", salt)
     await queryInterface.bulkInsert('Users', [
       {
         email: "ammar@mail.com",
         password: hash,
         fullName:"ammar",
         role:"user",
         createdAt: new Date(),
         updatedAt: new Date()
       }
     ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('User', {}, { truncate: true, restartIdentity: true })
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
