const express = require('express')
const router = express.Router()
const authRoutes = require("./user")
const cmsRoutes = require("./cms")

router.use('/app',authRoutes)
router.use('/cms',cmsRoutes)

module.exports = router