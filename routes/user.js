const express = require('express')
const router = express.Router()
const AuthController = require('../controller/auth')
const MoviesAndWishListController = require('../controller/movies')
const {Movies, Users} = require("../models");
const jwt = require('jsonwebtoken')
const { body, validationResult } = require('express-validator');
const multer = require('multer')
const storage = require('../services/multer_storage')
const upload = multer(
  {
    storage: storage,
    fileFilter: (req, file, cb) => {
      if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true)
      } else {
        cb(new Error('File should be an image'), false)
      }
    },
    limits: {
      fileSize: 3000
    }
  }
)


router.post('/login', AuthController.login)
router.post('/register',
upload.single('image'),
[
  body('fullName')
  .notEmpty()
  .withMessage('fullname should not be empty'),
  body('password')
  .notEmpty(),
  body("email")
  .notEmpty()
  .isEmail()
  .custom(async (email) => {
    const existingUser =
        await Users.findOne({where: {email} })
         
    if (existingUser) {
        throw new Error('Email already in use')
    }
}),
],
(req, res, next) => {
const errors = validationResult(req);
if (!errors.isEmpty()) {
  next({
    status: 400,
    message: errors.array()
  })
} else {
  next()
}
},
AuthController.register)

router.post('/forgot-password', AuthController.sendForgotPasswordToken)
router.post('/create-wishlist',
[
    body('movie_id')
      .optional()
      .notEmpty()
      .withMessage('movie ID should not be empty')
      .bail()
      .custom(value => {
        return Movies.findOne({
          where: {
            id: value,
          }
        }).then(movie => {
          if (!movie) {
            return Promise.reject('movie does not exist');
          }
        });
      })
      ,
      body('categori_id')
      .optional()
      .notEmpty(),
  ], 
(req, res, next) => {
    if (req.headers.authorization) {
      const decodedToken = jwt.decode(req.headers.authorization)
      req.user = decodedToken;
      next();
    } else {
      throw {
        status: 401,
        message: 'Unauthorized'
      }
    }
  }, MoviesAndWishListController.createWishList)

router.get('/get-wishlist', 
(req, res, next) => {
    if (req.headers.authorization) {
      const decodedToken = jwt.decode(req.headers.authorization)
      req.user = decodedToken;
      next();
    } else {
      throw {
        status: 401,
        message: 'Unauthorized'
      }
    }
  }, MoviesAndWishListController.getMovieWishList)

  router.put('/update-wishlist/:id',
[
    body('movie_id')
      .optional()
      .notEmpty()
      .withMessage('movie ID should not be empty')
      .bail()
      .custom(value => {
        return Movies.findOne({
          where: {
            id: value,
          }
        }).then(movie => {
          if (!movie) {
            return Promise.reject('movie does not exist');
          }
        });
      })
      ,
      body('categori_id')
      .optional()
      .notEmpty(),
  ], 
(req, res, next) => {
    if (req.headers.authorization) {
      const decodedToken = jwt.decode(req.headers.authorization)
      req.user = decodedToken;
      next();
    } else {
      throw {
        status: 401,
        message: 'Unauthorized'
      }
    }
  }, MoviesAndWishListController.updateWishList)

  router.delete('/wishlist/:id',(req, res, next) => {
    if (req.headers.authorization) {
        const decodedToken = jwt.decode(req.headers.authorization)
        req.user = decodedToken;
        next();
     } else {
        throw {
          status: 401,
          message: 'Unauthorized'
        }
      }
    }, MoviesAndWishListController.deleteWishList)

module.exports = router