const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const CmsController =  require('../controller/cms')
const {Movies, Users} = require("../models");
const { body, validationResult } = require('express-validator');
const multer = require('multer')
const storage = require('../services/multer_storage')
const upload = multer(
  {
    storage: storage,
    fileFilter: (req, file, cb) => {
      if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true)
      } else {
        cb(new Error('File should be an image'), false)
      }
    },
    limits: {
      fileSize: 3000
    }
  }
)


router.post("/register-cms",
[
    body('fullName')
    .notEmpty()
    .withMessage('fullname should not be empty'),
    body('password')
    .notEmpty(),
    body("email")
    .notEmpty()
    .isEmail()
    .custom(async (email) => {
      const existingUser =
          await Users.findOne({where: {email} })
           
      if (existingUser) {
          throw new Error('Email already in use')
      }
  }),
  ],
  (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    next({
      status: 400,
      message: errors.array()
    })
  } else {
    next()
  }
  }, 
(req, res, next) => {
if (req.headers.authorization) {
    const decodedToken = jwt.decode(req.headers.authorization)
    req.user = decodedToken;
    console.log(req.user.role)
    if (req.user.role != 'superadmin'){
        throw {
          status: 401,
          message: 'User is not superadmin'
        }
    }
    next();
  } else {
    throw {
      status: 401,
      message: 'Unauthorized'
    }
  }
},
CmsController.createAdmin)
router.post("/login-cms", CmsController.login)
router.post('/create-movie', (req, res, next) => {
    if (req.headers.authorization) {
      const decodedToken = jwt.decode(req.headers.authorization)
      req.user = decodedToken;
      console.log(req.user.role)
      if (req.user.role != 'admin'){
          throw {
            status: 401,
            message: 'User is not admin'
          }
      }
      next();
    } else {
      throw {
        status: 401,
        message: 'Unauthorized'
      }
    }
  }, 
  upload.single('image'),
  CmsController.createMovie)

router.post('/create-movie/:id',
(req, res, next) => {
    if (req.headers.authorization) {
      const decodedToken = jwt.decode(req.headers.authorization)
      req.user = decodedToken;
      console.log(req.user.role)
      if (req.user.role != 'admin'){
          throw {
            status: 401,
            message: 'User is not admin'
          }
      }
      next();
    } else {
      throw {
        status: 401,
        message: 'Unauthorized'
      }
    }
  },
CmsController.deleteMovie)
router.post('/create-movie/:id',
(req, res, next) => {
    if (req.headers.authorization) {
      const decodedToken = jwt.decode(req.headers.authorization)
      req.user = decodedToken;
      console.log(req.user.role)
      if (req.user.role != 'admin'){
          throw {
            status: 401,
            message: 'User is not admin'
          }
      }
      next();
    } else {
      throw {
        status: 401,
        message: 'Unauthorized'
      }
    }
  },
CmsController.updateMovie)


module.exports = router